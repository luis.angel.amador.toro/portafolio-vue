import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Gallery from "../views/GalleryView.vue";
import ContactView from "../views/ContactView.vue";

const routes = [
  { path: "/", redirect: "/inicio"},
  { path: "/inicio", name: "Home", component: Home },
  { path: "/galeria", name: "GalleryView", component: Gallery },
  { path: "/contacto", name: "Contacto", component: ContactView },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
