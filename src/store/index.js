import { createStore } from "vuex";

export default createStore({
  state: {
    arrayImages: [
      {
        id: 1,
        imageNumber: "1",
        msg: "Conocimientos necesarios y fundamentales de Javascript",
        title: "Manejo de",
        subtitle: "Javascript",
      },
      {
        id: 2,
        imageNumber: "2",
        msg: "Utilización de Bootstrap para la creación de páginas web (No soy bueno en CSS ni algún post-procesador)",
        title: "Manejo de",
        subtitle: "Bootstrap",
      },
      {
        id: 3,
        imageNumber: "3",
        msg: "Conocimientos necesarios y fundamentales de HTML5",
        title: "Manejo de",
        subtitle: "HTML",
      },
      {
        id: 4,
        imageNumber: "4",
        msg: "Desarrollo de aplicaciones ASP.NET 6 como backend en páginas web",
        title: "Manejo de",
        subtitle: "ASP.NET",
      },
      {
        id: 5,
        imageNumber: "5",
        msg: "Desarrollo de aplicaciones de windows forms con c#, conocimientos básicos y desarrollo de Xamarin Forms",
        title: "Manejo de",
        subtitle: "C#",
      },
      {
        id: 6,
        imageNumber: "6",
        msg: "Desarrollo de aplicaciones web SPA con React (Sin hooks), colaboré con manejo del DOM en www.eve9accessories.com.mx",
        title: "Manejo de",
        subtitle: "React(No Hooks)",
      },
      {
        id: 7,
        imageNumber: "7",
        msg: "Desarrollo de aplicaciones web SPA con VueJS y Vue 3(Esta página está hecha en Vue 3)",
        title: "Manejo de",
        subtitle: "Vue 3/VueJS",
      },
      {
        id: 8,
        imageNumber: "8",
        msg: "Desarrollo de aplicaciones web SPA con Angular 13 (En la página de inicio puedes checar el repositorio tanto del frontend como del backend)",
        title: "Manejo de",
        subtitle: "Angular 13",
      },
    ],
  },
  mutations: {},
  actions: {},
  modules: {},
});
